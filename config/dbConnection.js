const { MongoClient } = require('mongodb');

const uri = 'mongodb+srv://username:password@cluster0.mongodb.net/projetodsw?retryWrites=true&w=majority';

module.exports = async () => {
    const client = new MongoClient(uri);

    try {
        await client.connect();
        console.log('Conectado ao MongoDB');
        return client.db();
    } catch (error) {
        console.error('Erro ao conectar ao MongoDB', error);
        throw error;
    }
};
