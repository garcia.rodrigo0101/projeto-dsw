let dbConnection = require("../../config/dbConnection");

module.exports = {
    getTarefas: async (db, callback) => {
        const collection = db.collection('tarefas');
        const result = await collection.find({}).toArray();
        callback(null, result);
    },
    addTarefa: async (tarefa, db, callback) => {
        const collection = db.collection('tarefas');
        await collection.insertOne(tarefa);
        callback(null, true);
    },
    alterTarefa: async (tarefa, db, callback) => {
        const collection = db.collection('tarefas');
        await collection.updateOne({ _id: tarefa._id }, { $set: tarefa });
        callback(null, true);
    },
    getTarefaEsp: async (id, db, callback) => {
        const collection = db.collection('tarefas');
        const result = await collection.find({ id_usuario: id }).toArray();
        callback(null, result);
    }
};
