const { home } = require('../controllers/home');
const { check, validationResult } = require('express-validator');
const { getEsp, getTarefaEsp, alterTarefa } = require('../controllers/home');
const dbConnection = require('../../config/dbConnection'); // Adicionada a importação

module.exports = {
    inicio: function (app) {
        app.get('/', function (req, res) {
            res.render('inicio.ejs');
        });
    },

    home: async function (app) {
        app.get('/tarefas', async function (req, res) {
            const db = await dbConnection();
            home(app, req, res, db);
        });
    },

    getEsp: (app) => {
        app.get('/pesquisar', function (req, res) {
            const query = req.query;
            getEsp(query.id, req, res);
        });
    },

    saveTarefa: (app) => {
        app.post('/tarefa/salvar', [
            check('descricao').isLength({ min: 1, max: 100 }).withMessage("Descricao não pode ser nula"),
        ], async function (req, res) {
            const validation = validationResult(req);
            console.log(JSON.stringify(validation.errors, null, 2));
            if (validation.errors.length > 0) {
                res.render('alterTarefa.ejs', { errors: validation.errors, tarefa: req.body });
            } else {
                const db = await dbConnection();
                await addTarefaController(req.body, db);
                res.redirect('/');
            }
        });
    },

    alterTarefa: (app) => {
        app.get('/alterar/salvar', [
            check('descricao').isLength({ min: 1, max: 100 }).withMessage("Descricao não pode ser nula"),
            check('id_usuario').isLength({ min: 1 }).withMessage('Digite o ID do usuario'),
        ], async function (req, res) {
            const validation = validationResult(req);
            if (validation.errors.length > 0) {
                res.render('alterTarefa.ejs', { errors: validation.errors, tarefa: req.body });
            } else {
                const db = await dbConnection();
                await alterTarefa(req.body, db);
                res.redirect('/');
            }
        });
    },

    getTarefaEsp: (app) => {
        app.get('/alterar', async function (req, res) {
            const query = req.query;
            const db = await dbConnection();
            console.log(query.id);
            getTarefaEsp(query.id, req, res, db);
        });
    },

    searchEsp: (app) => {
        app.get('/tarefaesp', function (req, res) {
            const query = req.query;
            console.log(query);
            getEsp(query.id, req, res);
        });
    }
};
